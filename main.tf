provider "aws" {
  region                  = "us-east-1"
  shared_credentials_file = "~/.aws/credentials"
  version                 = "~> 2.0"
}

terraform {
  backend "s3" {
    bucket = "hards-tfstate-terraform"
    key    = "terraform.tfstate"
    region = "us-east-1"
  }
}
