locals {
  app-name                = "${var.app-name}-${var.env}"
  container_name          = "${local.app-name}-container"
  autoscaling_resource_id = "service/${aws_ecs_cluster.this.name}/${aws_ecs_service.this.name}"

  tags = {
    Project   = local.app-name
    Env       = var.env
    ManegedBy = "Terraform"
    Owner     = "HaroldoDevOps"
  }
}