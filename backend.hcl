terraform {
  backend "s3" {
    bucket = "hards-tfstate-terraform"
    key    = "terraform.tfstate"
    region = "us-east-1"
  }
}